# mccahill/r-studio
#
# VERSION 1.3

FROM  ubuntu:16.04
MAINTAINER Mark McCahill "mark.mccahill@duke.edu"

# get R from a CRAN archive (we want the 3.5 version of R)
RUN  echo "deb http://cran.rstudio.com/bin/linux/ubuntu xenial-cran35/" >> /etc/apt/sources.list
RUN DEBIAN_FRONTEND=noninteractive apt-key adv  --keyserver keyserver.ubuntu.com --recv-keys  E084DAB9

RUN apt-get  update ; \
    apt-get  dist-upgrade -y 

# we want OpenBLAS for faster linear algebra as described here: http://brettklamer.com/diversions/statistical/faster-blas-in-r/
RUN apt-get install  -y \
   apt-utils \
   libopenblas-base

RUN apt-get update ; \
   DEBIAN_FRONTEND=noninteractive apt-get  install -y  \
   r-base \
   r-base-dev \
   vim \
   less \
   net-tools \
   inetutils-ping \
   curl \
   git \
   telnet \
   nmap \
   socat \
   python-software-properties \
   wget \
   sudo \
   libcurl4-openssl-dev \
   libxml2-dev 

# we need TeX for the rmarkdown package in RStudio, and pandoc is also useful 
RUN apt-get update 
RUN DEBIAN_FRONTEND=noninteractive apt-get  install -y \
   texlive \
   texlive-base \
   texlive-latex-extra \
   texlive-pstricks \
   texlive-publishers \
   texlive-fonts-extra \
   texlive-humanities \
   lmodern \
   pandoc \
# dependency for R XML library
  libxml2 \ 
  libxml2-dev \
  libssl-dev 

# R-Studio
RUN DEBIAN_FRONTEND=noninteractive apt-get  install -y \
   gdebi-core \
   libapparmor1
   
# RUN DEBIAN_FRONTEND=noninteractive wget https://download2.rstudio.org/rstudio-server-1.1.383-amd64.deb
RUN DEBIAN_FRONTEND=noninteractive wget https://s3.amazonaws.com/rstudio-ide-build/server/trusty/amd64/rstudio-server-1.2.907-amd64.deb
RUN DEBIAN_FRONTEND=noninteractive gdebi --n rstudio-server-1.2.907-amd64.deb
RUN rm rstudio-server-1.2.907-amd64.deb

# update the R packages we will need for knitr
#RUN DEBIAN_FRONTEND=noninteractive wget \
#   https://mirrors.nics.utk.edu/cran/src/contrib/highr_0.7.tar.gz \
#   https://mirrors.nics.utk.edu/cran/src/contrib/formatR_1.5.tar.gz \
#   https://mirrors.nics.utk.edu/cran/src/contrib/evaluate_0.11.tar.gz 


#RUN DEBIAN_FRONTEND=noninteractive R CMD INSTALL \
#   highr_0.7.tar.gz \
#   formatR_1.5.tar.gz \
#   evaluate_0.11.tar.gz 
 

#RUN rm \
#   highr_0.7.tar.gz \
#   formatR_1.5.tar.gz \
#   evaluate_0.11.tar.gz 


# install packages via R scripts found in conf directory
ADD ./conf /r-studio   

# installs
RUN R CMD BATCH /r-studio/install-devtools.R
RUN rm /install-devtools.Rout 

# more OS-level libraries
RUN apt-get update 
RUN DEBIAN_FRONTEND=noninteractive apt-get  install -y \
  libfreetype6-dev

RUN /usr/bin/R -e 'options(warn=2); install.packages(c(  \
    "R.cache", \
    "R.methodsS3", \
    "R.oo", \
    "R.rsp", \
    "R.utils" \
    ), repos="http://cran.us.r-project.org")'

RUN /usr/bin/R -e 'options(warn=2); install.packages(c( \
    "tidymodels", \
    "tidypredict", \
    "tidyr", \
    "tidyselect", \
    "tidytext", \
    "tidygraph", \
    "tidyverse" \
    ), repos="http://cran.us.r-project.org")'

RUN /usr/bin/R -e 'options(warn=2); install.packages(c( \
    "statmod", \
    "XML", \
    "car", \
    "dbplyr", \
    "dplyr", \
    "dygraphs", \
    "evaluate", \
    "forecast", \
    "formatR" \
    ), repos="http://cran.us.r-project.org")'


RUN apt-get update 
RUN DEBIAN_FRONTEND=noninteractive apt-get  install -y \
    libudunits2-dev

RUN /usr/bin/R -e 'options(warn=2); install.packages(c( \
    "gdata", \
    "ggforce", \
    "ggformula", \
    "ggmap", \
    "ggplot2", \
    "ggraph", \
    "ggrepel", \
    "ggridges", \
    "ggstance"  \
    ),  repos="http://cran.us.r-project.org")'

RUN /usr/bin/R -e 'options(warn=2); install.packages(c(  \
    "gmodels", \
    "gtable", \
    "gtools", \
    "import", \
    "jpeg"  \
    ),  repos="http://cran.us.r-project.org")'

RUN /usr/bin/R -e 'options(warn=2); install.packages(c(  \
    "matrixStats", \
    "modelr", \
    "modeltools", \
    "mosaic", \
    "mosaicData", \
    "mvtnorm"  \
    ),  repos="http://cran.us.r-project.org")'
  
RUN /usr/bin/R -e 'options(warn=2); install.packages(c(  \
    "nycflights13", \
    "openintro", \
    "plyr", \
    "png", \
    "purrr", \
    "readr", \
    "readxl"  \
    ), repos="http://cran.us.r-project.org")'
  
RUN /usr/bin/R -e 'options(warn=2); install.packages(c(  \
    "forecast", \
    "timeDate", \
    "tinytex", \
    "xml2", \
    "yaml"  \
    ), repos="http://cran.us.r-project.org")'

RUN /usr/bin/R -e 'options(warn=2); install.packages(c(  \
    "markdown", \
    "rmarkdown", \
    "knitr", \
    "lubridate", \
    "jpeg", \
    "cowplot",\
    "downloader",\
    "import"  \
    ),  repos="http://cran.us.r-project.org")'

# dependencies=TRUE ,
	   
# Supervisord
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y supervisor && \
   mkdir -p /var/log/supervisor
CMD ["/usr/bin/supervisord", "-n"]

# Config files
RUN cd /r-studio && \
    cp supervisord-RStudio.conf /etc/supervisor/conf.d/supervisord-RStudio.conf
RUN rm /r-studio/*

# the default packages for everyone running R
RUN echo "" >> /etc/R/Rprofile.site && \
    echo "# add the downloader package to the default libraries" >> /etc/R/Rprofile.site && \
    echo ".First <- function(){" >> /etc/R/Rprofile.site && \ 
    echo "library(downloader)" >> /etc/R/Rprofile.site && \
    echo "library(knitr)" >> /etc/R/Rprofile.site && \ 
    echo "library(rmarkdown)" >> /etc/R/Rprofile.site && \
    echo "library(ggplot2)" >> /etc/R/Rprofile.site && \
    echo "library(lubridate)" >> /etc/R/Rprofile.site && \
    echo "library(stringr)" >> /etc/R/Rprofile.site && \
    echo "library(rvest)" >> /etc/R/Rprofile.site && \
    echo "library(openintro)" >> /etc/R/Rprofile.site && \
    echo "library(broom)" >> /etc/R/Rprofile.site && \
    echo "library(GGally)" >> /etc/R/Rprofile.site && \
    echo "library(STA210)" >> /etc/R/Rprofile.site && \
    echo "library(cowplot)" >> /etc/R/Rprofile.site && \
    echo "}" >> /etc/R/Rprofile.site  && \
    echo "" >> /etc/R/Rprofile.site
	

# add a non-root user so we can log into R studio as that user; make sure that user is in the group "users"
RUN adduser --disabled-password --gecos "" --ingroup users guest 

# add a script that supervisord uses to set the user's password based on an optional
# environmental variable ($VNCPASS) passed in when the containers is instantiated
ADD initialize.sh /

# set the locale so RStudio doesn't complain about UTF-8
RUN apt-get install  -y locales 
RUN locale-gen en_US en_US.UTF-8
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure locales


#########
#
# if you need additional tools/libraries, add them here.
# also, note that we use supervisord to launch both the password
# initialize script and the RStudio server. If you want to run other processes
# add these to the supervisord.conf file
#
#########

# expose the RStudio IDE port
EXPOSE 8787 

# expose the port for the shiny server
#EXPOSE 3838

CMD ["/usr/bin/supervisord"]
