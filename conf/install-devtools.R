r <- getOption("repos")
r["CRAN"] <- "http://cran.r-project.org"
options(repos=r)

utils::install.packages("devtools", dependencies=TRUE )
devtools::install_github('matackett/sta210/STA210')

#devtools::install_github('rstudio/markdown', dependencies=TRUE )
# devtools::install_github('rstudio/rmarkdown', dependencies=TRUE )
# devtools::install_github('rstudio/knitr', dependencies=TRUE )
# devtools::install_github('rstudio/lubridate', dependencies=TRUE )
